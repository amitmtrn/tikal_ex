import React, { useEffect, useRef } from 'react';
import _ from 'lodash';

export function Chart(props: {data: {text: string, value: number}[]}) {
  const canvasRef = useRef<HTMLCanvasElement>(null);
  
  useEffect(() => {
    const canvas = canvasRef.current;
    if(!(canvas instanceof HTMLCanvasElement)) throw new Error('could not find canvas')
    const context = canvas.getContext('2d');
    if(!(context instanceof CanvasRenderingContext2D)) throw new Error('could not create context')
    
    const columnArea = context.canvas.width / props.data.length;
    const maxHeight = _.maxBy(props.data, ((v: {text: string, value: number}) => v.value))?.value || context.canvas.height;
    context.fillStyle = '#FFFFFF'
    context.textAlign = "center"; 
    
    // clear the canvas
    context.fillRect(0, 0,  context.canvas.width, context.canvas.height);
    context.fillStyle = '#000000'
    const paddingTop = 15;
    const paddingBottom = 15;
    const paddingLeft = 0.15;
    const paddingRight = 0.15;
    const textPaddingBottom = 2;

    // draw columns
    _.each(props.data, ({text, value}, i) => {
      const columnHeight = (value / maxHeight * (context.canvas.height - paddingTop - paddingBottom));
      const x = i * columnArea;
      const center = x + (columnArea / 2);
      const y = context.canvas.height - columnHeight - paddingTop;

      context.strokeRect(x + (columnArea * paddingLeft), y, columnArea - (columnArea*(paddingLeft + paddingRight)), columnHeight);
      context.fillText(text, center, context.canvas.height);
      // replace for comma insertion
      context.fillText(value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","), center, y - textPaddingBottom);
    })

  }, [props.data])
  
  return (
    <div>
      <canvas width="900" height="700" ref={canvasRef} />
    </div>
  )
}