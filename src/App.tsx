import React, { useEffect, useState } from 'react';
import _ from 'lodash';
import './App.css';
import * as SWApi from './lib/SWApi';
import { Chart } from './lib/chart';

interface IGraphPlanets {
  text: string;
  value: number;
}

interface ITableVehicle {
  name: string;
  pilots: string[];
  population: {
      name: string;
      population: string;
  }[];
  readonly populationSum: number;
}

function App() {
  const [vehicle, setVehicle] = useState<ITableVehicle | {}>({});
  const [planets, setPlanets] = useState<IGraphPlanets[]>([]);

  useEffect(() => {
    SWApi.Planets.findBySearch(['Tatooine', 'Alderaan', 'Naboo', 'Bespin', 'Endor'])
      .then(planets => _.map(planets.resources, planet => ({text: planet.value.name, value: parseInt(planet.value.population)})))
      .then(planets => setPlanets(planets));

    SWApi.Vehicles.find(vehicle => vehicle.pilots.length > 0)
      .then(vehicles => vehicles.populateAll('pilots'))
      .then(vehicles => vehicles.populateAll('pilots.homeworld'))
      .then(vehicles => _.filter(vehicles.resources, vehicle => _.every(vehicle.value.pilots, (pilot => _.get(pilot, 'homeworld.population') !== 'unknown'))))
      .then(vehicles => _.map(vehicles, vehicle => ({
        name: vehicle.value.name,
        pilots: _.map(vehicle.value.pilots as SWApi.IPeople[], 'name'),
        population: _.map(vehicle.value.pilots, (pilot => ({name: _.get(pilot, 'homeworld.name'), population: _.get(pilot, 'homeworld.population')}))),
        get populationSum() {
          return _.sumBy(this.population, p => parseInt(p.population))
        }
      })))
      .then(vehicles => _.reverse(_.sortBy(vehicles, vehicle => vehicle.populationSum)))
      .then(vehicles => setVehicle(_.first(vehicles) || {}));
  }, [])
  return (
    <div className="App">
      <table className="max-table">
        <thead></thead>
        <tbody>
          <tr>
            <td>Vehicle name with the largest sum</td>
            <td>{_.get(vehicle,'name')}</td>
          </tr>
          <tr>
            <td>Related home planets and their respective population</td>
            <td>{JSON.stringify(_.get(vehicle,'population'))}</td>
          </tr>
          <tr>
            <td>Related pilot names</td>
            <td>{JSON.stringify(_.get(vehicle, 'pilots'))}</td>
          </tr>
        </tbody>
      </table>
      <Chart data={planets}/>
    </div>
  );
}

export default App;
